init:
    $ style.alt_days = Style(style.default)
    $ style.alt_days.color = "#390874"
    $ style.alt_days.italic = False
    $ style.alt_days.bold = True
    $ style.alt_days.size = 64
    $ style.alt_days.text_align = 0.5

    $ style.alt_chapters = Style(style.default)
    $ style.alt_chapters.color = "#2572ff"
    $ style.alt_chapters.italic = False
    $ style.alt_chapters.bold = True
    $ style.alt_chapters.size = 48
    $ style.alt_chapters.text_align = 0.5
    
    $ style.alt_credits = Style(style.default)
    $ style.alt_credits.color = "#EFF"
    $ style.alt_credits.drop_shadow = [ (1, 1), (1, 1), (1, 1), (1, 1) ]
    $ style.alt_credits.drop_shadow_color = "#111"
    $ style.alt_credits.italic = False
    $ style.alt_credits.bold = False
    $ style.alt_credits.text_align = 0.5
    image alt_credits = ParameterizedText(style = "alt_credits", size = 50)
    
    $ style.alt_letter = Style(style.default)
    $ style.alt_letter.color = "#00ffff"
    $ style.alt_letter.drop_shadow = [ (-1, 0), (0, 0), (-1, 1), (0, 1) ]
    $ style.alt_letter.drop_shadow_color = "#0ff"
    $ style.alt_letter.italic = True
    $ style.alt_letter.bold = False

    image alt_letter = ParameterizedText(style = "alt_letter", size = 70)

label alt_init_names:
    python:
        alt_init_name("me")
        alt_init_name("voice")
        alt_init_name("pi")
        alt_init_name("dreamgirl")
        alt_init_name("el")
        alt_init_name("un")
        alt_init_name("dv")
        alt_init_name("sl")
        alt_init_name("us")
        alt_init_name("mt")
        alt_init_name("cs")
        alt_init_name("mz")
        alt_init_name("mi")
        alt_init_name("uv")
        alt_init_name("sh")
    return

init -66 python:
    import random
    
    class alt_CardGameRival:
        
        def __init__(self,avatar,name):
            self.name = name
            self.mood = 0
            self.avatar = avatar
        
        def pick_my_card_last(self):
            for i in range(0,n_cards):
                if  cards_my[i].interesting:
                    x = i
            return x
        
        def allow_to_take(self):
            for i in range(0,n_cards):
                cards_rival[i].allow = True
        
        def allow_to_defend(self):
            return True
        
        def want_to_defend(self):
            return True
        
        def what_to_xchange(self):
            i = random.randrange(0,n_cards)
            j = random.randrange(0,n_cards)
            while i==j:
                j = random.randrange(0,n_cards)
            return (i,j)
        
        def give_away_card(self):
            return random.randrange(0,n_cards)
    
    class CardGameRivalSl(alt_CardGameRival):
        def pick_my_card(self):
            x = random.randrange(0,n_cards)
            while cards_my[x].name == name_of_none or cards_my[x].interesting:
                x = random.randrange(0,n_cards)
            return x
        
        def pick_my_card_last(self):
            return self.pick_my_card()
    
    class CardGameRivalSh(alt_CardGameRival):
        def pick_my_card(self):
            x = random.randrange(0,n_cards)
            while cards_my[x].name == name_of_none or cards_my[x].interesting:
                x = random.randrange(0,n_cards)
            return x
        
        def pick_my_card_last(self):
            return self.pick_my_card()
    
    class CardGameRivalMz(alt_CardGameRival):
        def pick_my_card(self):
            x = random.randrange(0,n_cards)
            while cards_my[x].name == name_of_none or cards_my[x].interesting:
                x = random.randrange(0,n_cards)
            return x
        
        def pick_my_card_last(self):
            return self.pick_my_card()
    
    class CardGameRivalMi(alt_CardGameRival):
        def pick_my_card(self):
            x = random.randrange(0,n_cards)
            while cards_my[x].name == name_of_none or cards_my[x].interesting:
                x = random.randrange(0,n_cards)
            return x
        
        def pick_my_card_last(self):
            return self.pick_my_card()
            
init -10 python:
    p = get_image_7dl("gui/avaset/sh/sh-")
    sh_avatar_set = {
                 'body':p+"body.png",
                 0     :p+"emo6.png",
            }
            
    p = get_image_7dl("gui/avaset/mi/mi-")
    mi_avatar_set = {
                 'body':p+"body.png",
                 0     :p+"emo5.png",
            }
            
    p = get_image_7dl("gui/avaset/mz/mz-")
    mz_avatar_set = {
                 'body':p+"body.png",
                 0     :p+"emo6.png",
            }
        
init -2 python:
    def make_names_unknown_7dl():
        global store
        set_name('ba',u"Физрук")
        set_name('ase',u"Алиса")
        set_name('we',u"Толпа")
        set_name('ml',u"Мальчик")
        set_name('ml2',u"Мальчик")
        set_name('alt_mt_voice',u"Голос")
        set_name('alt_voice',u"Голос")
        set_name('alt_voice1',u"Продавщица")
        set_name('kids',u"Дети")
        set_name('alt_me',u"Семён")
        set_name('dy',u"Динамики")
        set_name('alt_pi',u"Пионер")
        set_name('alt_dreamgirl',u"...")
        set_name('icq',u"Собеседник")
        set_name('voices',u"Голоса")
        set_name('alt_el',u"Кудрявый")
        set_name('alt_un',u"Грустяша")
        set_name('alt_dv',u"Рыжая")
        set_name('alt_sl',u"Блондинка")
        set_name('alt_us',u"Мелкая")
        set_name('alt_mt',u"Вожатая")
        set_name('alt_cs',u"Медсестра")
        set_name('alt_mz',u"Очкарик")
        set_name('alt_mi',u"Японка")
        set_name('alt_uv',u"Котэ")
        set_name('alt_sh',u"Очкарик")
    
    def alt_init_name(who):
        gl = globals()
        global store
        store.names["alt_"+who] = store.names[who]
        gl["alt_"+who+"_name"] = store.names[who]
        store.names_list.append("alt_"+who)
        colors["alt_"+who] = colors[who]

    def alt_meet(who, name):
        set_name(who,name)

    def set_name(who,name):
        gl = globals()
        global store
        store.names[who] = name
        gl[who+"_name"] = store.names[who]

init 265 python: 
    #Пресеты с возможностью настройки
    def Noir(id, brightness = -0.4, tint_r = 0.2126, tint_g = 0.7152, tint_b = 0.0722, saturation = 0.5):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(brightness) * im.matrix.tint(tint_r, tint_g, tint_b) * im.matrix.saturation(saturation))
    def D3_intro(id, brightness = -0.2, opacity = 0.5):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(brightness) *  im.matrix.opacity(opacity))
    def Desat(id, brightness = -0.35, saturation = 0.5):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(brightness) *  im.matrix.saturation(saturation))
    def Desat1(id, brightness = -0.4, saturation = 0.35):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(brightness) *  im.matrix.saturation(saturation))
        
    #Пресеты без возможности настройки
    def SS_com(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(-0.2) * im.matrix.contrast(1.6) * im.matrix.saturation(0)* im.matrix.colorize("#0aa", "#000"))
        
    def SS_com_r(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(-0.2) * im.matrix.contrast(1.6) * im.matrix.saturation(0)* im.matrix.colorize("#a00", "#000"))
        
    def Sepia(id):
        return im.MatrixColor(ImageReference(id), im.matrix.saturation(0.15) * im.matrix.tint(1.0, .94, .76))
        
    #Тинты для разного времени суток    
    def Notch(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(-0.15) * im.matrix.saturation(0.5))
    def Dawn(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(-0.1) * im.matrix.tint(0.94, 0.82, 1.0))
    def Noon(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(0.2) * im.matrix.tint(1.0, 0.94, 0.82))
    def HomeCity(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(-0.1) * im.matrix.tint(0.82, 0.84, 1.0))
        
    #Издевательства над фонцами, цг и грейном
    def filmetile(bitmap, opacity=0.1):
        return im.Tile(im.Alpha(bitmap,opacity))
    
#Инструкция для дырявой головы:
#scene expression Noir("ext_bus1")   # параметры "по умолчанию"
#scene expression Noir("ext_bus1", brightness = -0.3, tint_r = 0.7, tint_g = 0.4, tint_b = 0.3, saturation = -0.3)  #меняем все параметры
#scene expression Noir("ext_bus1", brightness = -0.3, saturation = -0.3)  #меняем отдельные параметры

init -1000 python:
    # Фабрика спрайтов (Provided by UVAO)
    # Константы:
    # тонировка:
    alt_tint_night = im.matrix.tint(0.63, 0.78, 0.82)
    alt_tint_sunset = im.matrix.tint(0.94, 0.82, 1.0)
    # Дефолтный путь к спрайтам
    default_7dl_path = 'mods/scenario_alt/'
    alt_chapters_dict = dict (
        mi7dl      = "mi normal pioneer",
        mi7        = "mi sad pioneer",
        mi7dlbad   = "mi cry pioneer",
        mi7dlgood  = "mi happy pioneer",
        mi7rej     = "mi serious pioneer",
        mi7true    = "my shy pioneer",
        
        sl         = "sl normal pioneer",
        slbad      = "sl sad pioneer",
        slcas      = "sl smile2 dress",
        sltrue     = "sl shy sport",
        sl7dl      = "sl smile pioneer",
        sl7dlbad   = "sl cry pioneer",
        
        un         = "un normal pioneer",
        unbad      = "un sad pioneer",
        un7dl      = "un normal pioneer",
        un7dlbad   = "un sorrow modern",
        un7dlgood  = "un smile modern",

        us7dl      = "us smile sport",
        
        mt7dl      = "mt grin pioneer",
        mt7dl_bad  = "mt sad pioneer",
        
        uv_unknown = "uv black silhouette",
        uv         = "uv normal",
        uv_true    = "uv surprise",
        uv_false   = "uv grin",
        uv_bad     = "uv guilty",
        
        dv         = "dv normal pioneer2",
        dvbad      = "dv sad pioneer2",
        db7dl      = "dv normal pioneer",
        dv7dlbad   = "dv guilty pioneer",
        dv7dlgood  = "dv smile pioneer",
    )
        

init -6 python:
    def alt_chapter0():
        global save_name
        save_name = (u"7ДЛ v.%s: пролог. %s") % (alt_release_no, alt_roles_dict[alt_role])

        
init -5 python:
    def alt_chapter(alt_day_number, alt_chapter_name):
        global save_name
        renpy.block_rollback()
        renpy.scene()
        if persistent.sprite_time == "day":
            renpy.show('bg ext_stand_2')
        elif persistent.sprite_time == "sunset":
            renpy.show('bg ext_stand_3_sunset')
        elif persistent.sprite_time == "night":
            renpy.show('bg ext_stand_3_night')
        elif persistent.sprite_time == "prolog":
            renpy.show('bg ext_stand_3_prolog')
        renpy.pause(1.0)
        renpy.transition(dissolve)
        if not routetag in alt_chapters_dict:
            renpy.show("owl")
            renpy.pause(0.3)
        else:
            renpy.show(alt_chapters_dict[routetag], at_list=[left])

        
        dn = (u"7ДЛ:День %d") % (alt_day_number)
# ----------------------------------------------------------------------
# в имя сохраняемого файла добавим номер релиза игры
        sdn = (u"7ДЛ v.%s :День %d") % (alt_release_no, alt_day_number)
# -----------------------------------------------------------------------
        save_name = ((sdn) + (u" - ")) + (alt_chapter_name)
        if persistent.sprite_time == "prolog":
            renpy.show('day_num', what=Text(dn, style=style.alt_days,xcenter=0.5215,ycenter=0.25))
            renpy.show('day_text', what=Text(alt_chapter_name, style=style.alt_chapters,xcenter=0.5215,ycenter=0.35))
        else:
            renpy.show('day_num', what=Text(dn, style=style.alt_days,xcenter=0.5215,ycenter=0.35))
            renpy.show('day_text', what=Text(alt_chapter_name, style=style.alt_chapters,xcenter=0.5215,ycenter=0.45))
        
        renpy.pause(3)
        renpy.scene()
        renpy.show('bg black')
        renpy.transition(blind_r)
        set_mode_adv()

        
        
        
    if persistent.altCardsDemo == None:
        persistent.altCardsDemo = False

    if persistent.altCardsFail == None:
        persistent.altCardsFail = False

    if persistent.altCardsWon1 == None:
        persistent.altCardsWon1 = False

    if persistent.altCardsWon2 == None:
        persistent.altCardsWon2 = False

    if persistent.altCardsWon3 == None:
        persistent.altCardsWon3 = False
        
    if persistent.altCardsWon4 == None:
        persistent.altCardsWon4 = False
    
    
    # Функция для дрожания огонька спички в котокомбах
    def random_zoom(trans, st, at):
        if st < 1.0: # 1 sec random zooming each 0.1 sec
            trans.zoom = 1.0 + renpy.random.random() * 0.5 
            return 0.1
        trans.zoom = 1.0
        return None        

    # Фабрика спрайтов (Provided by UVAO)
    # Константы:
    # тонировка:
    alt_tint_night = im.matrix.tint(0.63, 0.78, 0.82)
    alt_tint_sunset = im.matrix.tint(0.94, 0.82, 1.0)    
    # Простая функция, строит спрайт из переданных путей
    def ComposeSprite(*argv):
        #строим аргументы для im.Composite
        subargs = list()
        for arg in argv:
           subargs.append( (0,0) )
           subargs.append( arg )
        sprite = im.Composite(None, *subargs)
        return ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(sprite, alt_tint_sunset), "persistent.sprite_time=='night'", im.MatrixColor(sprite, alt_tint_night), True, sprite)
    # /ComposeSprite(*argv)

    # Функция, собирающая спрайты из запчастей
    # types - набор калибров спрайтов. Любой набор из ('far', 'close', 'normal', 'veryfar'). По пути, где лежат спрайты, должны быть соотвествующие директории, иначе не найдет
    # argv - файлы-запчасти. передаются в формате ('path', 'file') - например ('images/sprites/','dv/dv_1_coat.png'), или просто 'file' - тогда используется alt_default_sprites_path
    # на выходе - dict спрайтов, по одному для каждого из types
    def ComposeSpriteSet(distance, *argv):
        if isinstance(distance, str): #если types содержит только один параметр.
            distances = (distance,) # 1-tuple. Иначе for будет перебирать символы в строке.
        else:
            distances = distance
        ret = dict()    
        for dst in distances:
            #строим аргументы для ComposeSprite
            subargs = list()
            for arg in argv:
                if isinstance(arg, str): #просто file
                    subarg = (default_7dl_path+"Sprites/", arg)
                else: # (path, file)
                    subarg = arg;
                subargs.append( subarg[0] + '/' + dst +'/' + subarg[1] ) # 'images/sprites/normal/dv/dv_1_coat.png'
            ret[dst] = ComposeSprite(*subargs)
        return ret
    # /ComposeSpriteSet(type, *argv)

    def get_image_extra7dl(file):
        return default_7dl_path+"pics_extra/%s" % (file)

init -1001 python:
    def disable_all_chibi():
        global global_zones
        for name,data in global_zones.iteritems():
            data["map_chibi"] = None
            
init -999 python:
    def get_image_7dl(file):
        return default_7dl_path+"Pics/%s" % (file)
        
init -998 python:
    def get_sound_7dl(file):
        return default_7dl_path+"Sound/%s" % (file)
    def get_sfx_7dl(file):
        return get_sound_7dl("sfx/%s" % (file))
    def get_ambience_7dl(file):
        return get_sound_7dl("ambience/%s" % (file))
    def get_music_7dl(file):
        return get_sound_7dl("music/%s" % (file))
        
init -997 python:
    def get_sprite_extra_7dl(file):
        return get_image_extra7dl("sprites/%s" % (file))
    def get_sprite_7dl(file):
        return get_image_7dl("sprites/%s" % (file))
    def get_sprite_ori(file):
        return get_image("sprites/%s" % (file))

    
    store.map_chibi = {
        "?" : get_image_7dl("gui/maps/map_icon_n00.png"),
        "me": get_image_7dl("gui/maps/map_icon_n01.png"),
        "mi": get_image_7dl("gui/maps/map_icon_n02.png"),
        "sh": get_image_7dl("gui/maps/map_icon_n03.png"),
        "el": get_image_7dl("gui/maps/map_icon_n04.png"),
        "mz": get_image_7dl("gui/maps/map_icon_n05.png"),
        "mt": get_image_7dl("gui/maps/map_icon_n06.png"),
        "uv": get_image_7dl("gui/maps/map_icon_n07.png"),
        "un": get_image_7dl("gui/maps/map_icon_n08.png"),
        "us": get_image_7dl("gui/maps/map_icon_n09.png"),
        "dv": get_image_7dl("gui/maps/map_icon_n10.png"),
        "sl": get_image_7dl("gui/maps/map_icon_n11.png"),
        "cs": get_image_7dl("gui/maps/map_icon_n12.png"),
    }
        
init python:
    import math
    class Shaker(object):
        anchors = {
            'top' : 0.0,
            'center' : 0.5,
            'bottom' : 1.0,
            'left' : 0.0,
            'right' : 1.0,
        }
    
        def __init__(self, start, child, dist):
            if start is None:
                start = child.get_placement()
            #
            self.start = [ self.anchors.get(i, i) for i in start ]  # central position
            self.dist = dist    # maximum distance, in pixels, from the starting point
            self.child = child
            
        def __call__(self, t, sizes):
            # Float to integer... turns floating point numbers to
            # integers.                
            def fti(x, r):
                if x is None:
                    x = 0
                if isinstance(x, float):
                    return int(x * r)
                else:
                    return x

            xpos, ypos, xanchor, yanchor = [ fti(a, b) for a, b in zip(self.start, sizes) ]

            xpos = xpos - xanchor
            ypos = ypos - yanchor
            
            nx = xpos + (1.0-t) * self.dist * (renpy.random.random()*2-1)
            ny = ypos + (1.0-t) * self.dist * (renpy.random.random()*2-1)

            return (int(nx), int(ny), 0, 0)
    
    def _Shake(start, time, child=None, dist=100.0, **properties):

        move = Shaker(start, child, dist=dist)
    
        return renpy.display.layout.Motion(move,
                      time,
                      child,
                      add_sizes=True,
                      **properties)

    Shake = renpy.curry(_Shake)

    def not_in_rollback_or_fast_forward():
            return not renpy.config.skipping and not renpy.game.after_rollback

