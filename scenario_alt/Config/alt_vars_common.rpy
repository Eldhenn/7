label alt_init_vars:
    $ sfx_blanket_off = "sound/sfx/blanket_off.ogg"
    $ th_prefix = "«"
    $ th_suffix = "»"
    $ timeskip_come = "Ты пойдёшь со мной?"
    $ timeskip_dev = "Рут находится в разработке…\nВ активной разработке: д.5 Одиночка. Готовность 80%%."
    $ timeskip3 = "Я скучаю…"
    $ timeskip4 = "Я хочу к тебе…"
    $ timeskip5 = "ПОШЛА ВОООООН!"
    $ timeskip6 = "ПРЕДУПРЕЖДЕНИЕ\nВсе совпадения персонажей и характеров\nс реально существующими людьми\nсчитать злой волей автора"
    $ timeskip7 = "Твоя Мику"
    $ timeskip8 = "ПРОСНИСЬ"
    $ timeskip9 = "Я буду ждать."
    $ timeskip10 = "Спасибо…"
    $ timeskip11 = "Прощай."
    $ alt_credits_text = "Команда Soviet Games (IIchan Eroge Team) благодарит вас за время, уделённое игре!\n\nБлагодарности:\n\nPyTom'у за движок Ren'Py.\n\nСайту freesounds.org за бесплатные звуки.\n\nСайтам iichan.hk и 2ch.hk.\n\nВсем, кто помогал работать над игрой.\n\nВсем, кто нас поддерживал все эти годы, ждал и верил!\n\n7ДЛ\n\nBeta-test:\n\nМакс Ветров, Drago23, Arlien, Peregarrett, Demiurge-kun, Дельта, KirillZ89, Ленофаг Простой, Ленивый Бегун, Занудный, Serge Domingo, Ravsii, Dantiras.\n\nОСОБЫЕ БЛАГОДАРНОСТИ:\n\nMannych - за колоритного физрука!\n\nEldhenn - за адаптацию игры в Steam.\n\nPeregarrett, Chess - за кошко-рут.\n\nЛенофаг Простой, Ravsii - за идеи и помощь их реализации.\n\nNuttyprof, openplace - за новую карту лагеря.\n\nВсем, кого не упомянул, но не забыл - за то, что помогали и поддерживали!\n\n\n\nКОНЕЦ.\n"

    $ colors['ba'] = {'night': (88, 18, 67, 255), 'sunset': (132, 27, 100, 255), 'day': (252, 15, 192, 255), 'prolog': (150, 50, 100, 255)}
    $ names['ba'] = u'Физрук'
    $ store.names_list.append('ba')
    #Камео мисс Селезнёвой
    $ colors['ase'] = {'night': (97, 10, 10, 255), 'sunset': (137, 14, 14, 255), 'day': (236, 66, 66, 255), 'prolog': (137, 14, 14, 255)}
    $ names['ase'] = u'Алиса'
    $ store.names_list.append('ase')

    $ colors['we'] = {'night': (67, 23, 111, 255), 'sunset': (132, 27, 100, 255), 'day': (252, 15, 192, 255), 'prolog': (150, 50, 100, 255)}
    $ names['we'] = u'Все вместе'
    $ store.names_list.append('we')

    $ colors['ml'] = {'night': (20, 69, 68, 255), 'sunset': (32, 106, 104, 255), 'day': (74, 200, 147, 255), 'prolog': (32, 106, 106, 255)}
    $ names['ml'] = u'Мальчик'
    $ store.names_list.append('ml')

    $ colors['ml2'] = {'night': (20, 46, 68, 255), 'sunset': (32, 73, 104, 255), 'day': (74, 170, 147, 255), 'prolog': (32, 75, 106, 255)}
    $ names['ml2'] = u'Мальчик2'
    $ store.names_list.append('ml2')

    $ colors['voice1'] = {'night': (159, 8, 73, 255), 'sunset': (196, 7, 92, 255), 'day': (255, 136, 192, 255), 'prolog': (196, 7, 124, 255)}
    $ names['voice1'] = u'Продавщица'
    $ store.names_list.append('voice1')

    $ colors['icq'] = {'night': (6, 62, 14, 255), 'sunset': (10, 91, 20, 255), 'day': (10, 204, 10, 255), 'prolog': (10, 91, 30, 255)}
    $ names['icq'] = u'Собеседник'
    $ store.names_list.append('icq')
    #Резерв
    #$ colors['ann'] = {'night': (15, 159, 14, 255), 'sunset': (10, 217, 16, 255), 'day': (170, 254, 160, 255), 'prolog': (10, 215, 30, 255)}
    #$ store.names_list.append('ann')
    $ voices = Character(u'Голоса', color="#c0c0c0", what_color="C0C0C0", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000")
    $ kids = Character(u'Малышня', color="#eb7883", what_color="E2C778", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000")
    $ dy = Character(u'Голос из динамика', color="#c0c0ff", what_color="E2C778", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000")
    $ reload_names()

    $ mt_pt = 0
    $ d3_pt = 0
    $ us_pt = 0

    $ lp_el = 0
    $ lp_mi = 0
    $ lp_sl = 0
    $ lp_un = 0
    $ lp_us = 0
    $ lp_dv = 0

    $ karma = 0
    $ semen_str = 10
    $ alt_sp = 0

    $ herc = False
    $ loki = False
# ------------------------------------------------
# только если игру начали заново - принимаем номер релиза сохранения по номеру релиза мода

    $ alt_save_release_no = alt_release_no
    return

label alt_day0_init:
    python:
        alt_roles_dict = {
            'loki': u"Локи",
            "drisch"  : u'Дрищ',
            "dr"  : u'Дрищ',
            'herc': u'Герк',
            'choose': 'Выбор'
        }
        alt_role = 'choose'
        routetag = "prologue"
        role_bg = "intro_ground"
        alt_day1_random_val = 0
    return

label alt_day1_init:
    $ make_names_unknown_7dl()
    #Переменные первого дня
    $ alt_day1_alt_chase = False
    $ alt_day1_alt_us_robbed = False
    $ alt_day1_dv_feed = False
    $ alt_day1_el_followed = False
    $ alt_day1_sam_paniqued = False
    $ alt_day1_sl_met = False
    $ alt_day1_sl_conv = False
    $ alt_day1_sl_keys_took = 0
    $ alt_day1_un_dated = False
    $ alt_day1_un_ignored = False
    $ alt_day1_un_nicebook = False
    $ alt_day1_un_stopped = False
    $ alt_day1_us_shotted = False
    $ alt_day1_chase = False
    $ alt_day1_chase_uvao = False
    $ alt_day1_genda_investigation = False
    $ alt_day1_cofront_sl_dv = 0
    $ alt_day1_headshot = False
    return

label alt_day2_init:
    #Переменные второго дня
    $ alt_day2_bf_dv_us = False
    $ alt_day2_bf_un = False

    $ alt_day2_club_join_badmin = False
    $ alt_day2_club_join_cyb = False
    $ alt_day2_club_join_footbal = False
    $ alt_day2_club_join_nwsppr = False
    $ alt_day2_club_join_musc = False #И сходил, и записался в музклуб (включает в себя переменную muz_done)
    $ alt_day2_club_join_volley = False
    $ alt_day2_dv_bet_approve = False
    $ alt_day2_dv_bet_won = 0
    $ alt_day2_dv_bumped = False
    $ alt_day2_dv_chased = False
    $ alt_day2_dv_harass = False
    $ alt_day2_dv_tears = False
    $ alt_day2_dv_us_house_visited = False
    $ alt_day2_loki_minijack = False
    $ alt_day2_mi_kumuhimo = 0
    $ alt_day2_mi_met = False
    $ alt_day2_mi_met2 = False
    $ alt_day2_mi_rejected = False
    $ alt_day2_mt_help = False
    $ alt_day2_rendezvous = 0
    $ alt_day2_rendezvous_dinner = 0
    $ alt_day2_sl_bf = False
    $ alt_day2_sl_guilty = 0 #0 не был свидетелем, 1 был, 2 вступился
    $ alt_day2_sl_help = False
    $ alt_day2_date = 0 #un 1, sl 2, dv 3, mi 4, us 5, mt 6
    $ alt_day2_un_secret_spot = 0
    $ alt_day2_us_dubstep = False
    $ alt_day2_us_escape = False
    $ alt_day2_muz_done = False #Если только ходил в музклуб, но не записывался
    $ alt_day2_lib_done = False
    $ alt_day2_med_done = False
    $ alt_day2_club_done = False
    $ alt_day2_phys_done = False
    $ alt_day2_beach_done = False
    $ alt_day2_necessary_done = 0
    $ alt_day2_walk = 0
    $ alt_day2_fail = 0
    $ alt_day2_sup = 0
    $ alt_day2_cardgame_block_rollback = False
    $ alt_day2_olga_help = False
    #Переменные для турнира
    $ alt_day2_round1 = 0 #0 не участвовал, 1 проиграл, если победил то -V-
    $ alt_day2_round2 = 0 #0 не участвовал, 1 проиграл, если победил, то -V-
    $ alt_day2_round3 = 0 #0 не участвовал 1 для проигрыша 2 для победы
    $ alt_day2_desk1 = 0
    $ alt_day2_hf1 = 0
    $ alt_day2_desk2 = 0
    $ alt_day2_hf2 = 0
    $ alt_day2_f1 = 0
    $ alt_day2_desk3 = 0
    $ alt_day2_hf3 = 0
    $ alt_day2_desk4 = 0
    $ alt_day2_hf4 = 0
    $ alt_day2_f2 = 0
    $ alt_day2_revanche = False

    return

label alt_day3_init:
    #Переменные третьего дня
    $ alt_day3_dancing = 0
    $ alt_day3_duty = locals().get('alt_day3_duty', False)

    $ alt_day3_dv_date = False #Дейтим ДваЧе.
    $ alt_day3_dv_dj = False
    $ alt_day3_dv_evening = False
    $ alt_day3_dv_event = False
    $ alt_day3_dv2_event = False
    $ alt_day3_dv_guitar_lesson = False
    $ alt_day3_dv_invite = False
    $ alt_day3_dv_rejected = False
    $ alt_day3_dv_reunion = False

    $ alt_day3_ladder_phys = 0 #Статус готовности для стремянки физрука
    $ alt_day3_ladder_ph_loser = False #Был послан физруком нахрен
    $ alt_day3_ladder_mt = 0   #Статус готовности для деревянной лесенки мод-тян
    $ alt_day3_ladder_mt_loser = False #Малахольный гг не смог поднять лесенку.
    $ alt_day3_ladder_us = 0

    $ alt_day3_mi_date = False # Дейтим Мику, 2 танца мастхэв
    $ alt_day3_mi_dj = False
    $ alt_day3_mi_event = False
    $ alt_day3_mi_rejected = False
    $ alt_day3_mi_invite = False # Мику тащит одеваться и учиться танцевать
    $ alt_day3_mi_invite2 = False # Мику тащит одеваться и учиться танцевать - 2

    $ alt_day3_sl_bath_voy = False
    $ alt_day3_sl_day_event2 = False
    $ alt_day3_sl_day_event = False
    $ alt_day3_sl_event = False
    $ alt_day3_sl_invite = False

    $ alt_day3_technoquest_st1 = False
    $ alt_day3_technoquest_st2 = False
    $ alt_day3_technoquest_st3 = 0
    $ alt_timer = 10
    $ alt_day3_timer_range = 0
    $ alt_day3_timer_jump = 0

    $ alt_day3_lp_route = 0

    $ alt_day3_un_cheated = False
    $ alt_day3_un_date = False
    $ alt_day3_un_event = False
    $ alt_day3_un_med_help = False
    $ alt_day3_un_strip_pool_sp = 0 #Флаг райвалсов для стрип-партии - пул гг
    $ alt_day3_un_strip_pool_un = 0 #Флаг райвалсов для стрип-партии - пул унылки
    $ alt_day3_un_invite = 0 # Вторая часть ивента Унылки
    $ alt_day3_us_shenan = False
    $ alt_day3_us_bugs = 0
    $ alt_day3_us_invite = False # Ульянка зовёт собирать жуков.

    $ alt_day3_uvao_spotted = False #Заметили Юленьку у столовой
    return

label alt_common_init:
    call alt_day0_init
    call alt_day1_init
    call alt_day2_init
    call alt_day3_init
    return

label alt_init_maps:
# инициализация карт. Должна выполняться ТОЛЬКО один раз - иначе не работают сохранения
# ------------------------------------------------
    $ init_map_zones_alt1()
    $ init_map_zones_alt2()
    return

# ------------------------------------------------

screen intro_loki_screen:
    add "intro_loki" xalign 0.0 yalign 0.0

screen intro_herc_screen:
    add "intro_herc" xalign 0.0 yalign 0.0

screen intro_drisch_screen:
    add "intro_drisch" xalign 0.0 yalign 0.0

screen role_menu_7dl:
    tag menu
    modal False
    imagemap:
        ground "intro_transparent"
        hotspot ((0, 0, 635, 1080)):
            hovered [Show("intro_loki_screen", transition=Dissolve(0.5))]
            unhovered [Hide("intro_loki_screen", transition=Dissolve(1.0))]
            action [Hide("intro_loki_screen", transition=Dissolve(0.5)), Jump("alt_day0_chosen_loki")]
        hotspot ((635, 0, 652, 1080)):
            hovered [Show("intro_herc_screen", transition=Dissolve(0.5))]
            unhovered [Hide("intro_herc_screen", transition=Dissolve(1.0))]
            action [Hide("intro_herc_screen", transition=Dissolve(0.5)), Jump("alt_day0_chosen_herc")]
        hotspot ((1287, 0, 634, 1080)):
            hovered [Show("intro_drisch_screen", transition=Dissolve(0.5))]
            unhovered [Hide("intro_drisch_screen", transition=Dissolve(1.0))]
            action [Hide("intro_drisch_screen", transition=Dissolve(0.5)), Jump("alt_day0_chosen_drisch")]

