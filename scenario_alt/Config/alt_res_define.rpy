init:
    transform voy_left:
        xalign 0.0
        xanchor 0.5
        yanchor 0.0

    transform voy_right:
        xalign 1.0
        xanchor 0.5
        yanchor 0.0
        
    transform zenterright:
        xalign 0.5 yalign 0.5 zoom 1.0
        linear 0.4 zoom 1.02 xalign 0.7 yalign 0.5
        
    transform enterright:
        xalign 0.5 yalign 0.5 zoom 1.02
        linear 0.6 zoom 1.05 xalign 0.9 yalign 0.5
        
    transform zenterleft:
        xalign 0.5 yalign 0.5 zoom 1.0
        linear 0.4 zoom 1.02 xalign 0.3 yalign 0.5
        
    transform enterleft: #по левую
        xalign 0.5 yalign 0.5 zoom 1.02
        linear 0.6 zoom 1.05 xalign 0.1 yalign 0.5
        
    transform zentercenter: #для открывающихся дверей
        xalign 0.5 yalign 0.5 zoom 1.0
        linear 0.8 zoom 1.05 xalign 0.5 yalign 0.5
        
    transform zentercenter2: #для открывающихся дверей
        xalign 0.5 yalign 0.5 zoom 1.0 subpixel True
        linear 20.0 zoom 1.5 xalign 0.5 yalign 0.5
        
    transform zexitcenter: #отдаляющий эффект от центра
        xalign 0.5 yalign 0.5 zoom 1.05 subpixel True
        linear 0.8 zoom 1.0 xalign 0.5 yalign 0.5
        
    transform zexitcenter2: #отдаляющий эффект от центра
        xalign 0.5 yalign 0.5 zoom 1.5
        linear 20 zoom 1.0 xalign 0.5 yalign 0.5
        
    transform zexitright: #отдалаюящий эффект от левого края к центру
        xalign 0.7 yalign 0.5 zoom 1.05
        linear 0.8 zoom 1.0 xalign 0.5 yalign 0.5
        
    transform zexitleft: #отдаляющий эффект от правого края к центру 
        xalign 0.3 yalign 0.5 zoom 1.05
        linear 0.8 zoom 1.0 xalign 0.5 yalign 0.5
        
#Наши транзиты, с блекджеком и разными цветами.
    $ flash_cyan = Fade(1, 0, 1, color="#1fa")
    $ fade_red = Fade(2, 2, 2, color="#f11")
    $ flash_pink = Fade(1, 0, 1, color="#e25")
    
    $ diam = ImageDissolve(im.Tile(get_image_7dl("gui/pattern.jpg")), 1.1, 1)
    $ fdiam = ImageDissolve(im.Tile(get_image_7dl("gui/pattern.jpg")), 0.4, 1)
    $ fulldiam = MultipleTransition([False,fdiam,get_image_7dl("gui/digi1.jpg"),fdiam,True])
    
    $ swradar = ImageDissolve(im.Tile(get_image_7dl("gui/blackout3.jpg")), 0.95, 1)
    $ joff = MultipleTransition([False,swradar,Solid("#000"),swradar,True])
    $ swradarr = ImageDissolve(im.Tile(get_image_7dl("gui/blackout32.jpg")), 0.95, 1)
    $ joffr = MultipleTransition([False,swradarr,Solid("#000"),swradarr,True])
    
    $ blind_d = ImageDissolve(im.Tile(get_image_7dl("gui/roof_ks.jpg")), 1.3)
    $ blinds_l = ImageDissolve(im.Tile(get_image_7dl("gui/roof_ks2.jpg")), 0.6)
    $ blinds_r = ImageDissolve(im.Tile(get_image_7dl("gui/roof_ks3.jpg")), 0.7)
    
    $ blind_l = MultipleTransition([False,blinds_l,Solid("#011"),blinds_r,True])
    $ blind_r = MultipleTransition([False,blinds_r,Solid("#011"),blinds_l,True])
    
    $ touch = ImageDissolve(im.Tile(get_image_7dl("gui/pattern2.jpg")), 0.9, 1)

    
#Анимы
    image anim_digi:
        get_image_7dl("gui/digi1.jpg")  with Dissolve(1.5) 
        pause 1.5
        get_image_7dl("gui/digi2.jpg")  with Dissolve(1.5) 
        pause 1.5
        repeat
    
    image anim_grain: #Смэртельный номер: тянем картинку, делаем прозрачной и запускаем в секвенцию - и всё в коде!
        filmetile(get_image_7dl("gui/alt_noise1.png"))
        pause 0.1
        filmetile(get_image_7dl("gui/alt_noise2.png"))
        pause 0.1
        filmetile(get_image_7dl("gui/alt_noise3.png"))
        pause 0.1
        repeat
        
    image anim_intro_recall:
        get_image("bg/semen_room.jpg")
        pause 0.1
        get_image("bg/semen_room_window.jpg")
        pause 0.1
        get_image("anim/intro_1.jpg")
        pause 0.1
        get_image("anim/intro_2.jpg")
        pause 0.1
        get_image("anim/intro_3.jpg")
        pause 0.1
        get_image("anim/intro_4.jpg")
        pause 0.1
        get_image("anim/intro_5.jpg")
        pause 0.1
        get_image("anim/intro_6.jpg")
        pause 0.1
        get_image("anim/intro_8.jpg")
        pause 0.1
        get_image("anim/prolog_2.jpg")
        pause 0.1
        get_image("anim/prolog_1.jpg")
        pause 0.1
        get_image("anim/intro_9.jpg")
        pause 0.1
        get_image("anim/intro_10.jpg")
        pause 0.1
        get_image("anim/intro_11.jpg")
        pause 0.1
        get_image("anim/intro_13.jpg")
        pause 0.1
        get_image("bg/intro_xx.jpg")
        pause 0.1
        repeat
        
    image anim_square_party:
        get_image("bg/ext_square_night_party.jpg") with Dissolve(.5) 
        pause 0.6
        get_image("bg/ext_square_night_party2.jpg") with Dissolve(.5) 
        pause 0.6
        repeat
        
    image anim_square_preparty:
        get_image_7dl("bg/ext_square_sunset3.jpg") with Dissolve(1.5) 
        pause 1.6
        get_image_7dl("bg/ext_square_sunset2.jpg") with Dissolve(1.5) 
        pause 1.4
        repeat
    
    image uv_bus:
        get_sprite_7dl("misc/uv_alt1.png")
        pause 0.5
        get_sprite_7dl("misc/uv_alt2.png") 
        pause 0.5
        get_sprite_7dl("misc/uv_alt3.png")
        pause 0.5
        get_sprite_7dl("misc/uv_alt2.png")
        pause 0.5
        get_sprite_7dl("misc/uv_alt1.png")
        pause 0.5
    
    image anim_underwater:
        get_image_7dl("bg/ext_underwater.jpg")  with Dissolve(2.0) 
        pause 2.0
        get_image_7dl("bg/ext_underwater2.jpg")  with Dissolve(2.0) 
        pause 2.0
        get_image_7dl("bg/ext_underwater3.jpg")  with Dissolve(2.0) 
        pause 2.0
        get_image_7dl("bg/ext_underwater2.jpg")  with Dissolve(2.0) 
        pause 2.0
        get_image_7dl("bg/ext_underwater.jpg")  with Dissolve(2.0) 
        pause 2.0
        repeat
    
    image timer_anim: 
        get_image_7dl("gui/win.png") 
        0.1 #Задержка
        get_image_7dl("gui/win2.png")
        0.1
        get_image_7dl("gui/win3.png")
        0.1
        repeat # Не убирать
        
    image ftl_anim: 
        get_image_7dl("gui/ftl1.png") 
        0.1 #Задержка
        get_image_7dl("gui/ftl2.png")
        0.1
        get_image_7dl("gui/ftl3.png")
        0.1
        repeat # Не убирать
        
    image un serious dress anim: 
        get_sprite_7dl("misc/un_3_dress_serious_1.png") 
        8.0 #Задержка
        get_sprite_7dl("misc/un_3_dress_serious_2.png")
        0.1
        get_sprite_7dl("misc/un_3_dress_serious_1.png") 
        4.0 #Задержка
        get_sprite_7dl("misc/un_3_dress_serious_2.png")
        0.15
        get_sprite_7dl("misc/un_3_dress_serious_1.png") 
        7.0 #Задержка
        get_sprite_7dl("misc/un_3_dress_serious_2.png")
        0.1
        repeat # Не убирать
#Заставки
    image bg ext_stand_3_night = im.MatrixColor(get_image_7dl("bg/ext_stand3.jpg"), im.matrix.tint(0.63, 0.78, 0.82))
    image bg ext_stand_3_sunset = im.MatrixColor(get_image_7dl("bg/ext_stand3.jpg"), im.matrix.tint(0.94, 0.82, 1.0))
    image bg ext_stand_3_prolog = im.MatrixColor(get_image_7dl("bg/ext_stand_pr.jpg"), im.matrix.tint(0.82, 0.84, 1.0))
    
#Задники
    image bg ext_adductius = get_image_7dl("bg/ext_adductius.jpg")
    image bg ext_admins_day = get_image_7dl("bg/ext_admins_day.jpg")
    image bg ext_admins_night = get_image_7dl("bg/ext_admins_night.jpg")
    image bg ext_admins_rain = get_image_7dl("bg/ext_admins_rain.jpg")
    image bg ext_aidpost_sunset = get_image_7dl("bg/ext_med_sunset.jpg")
    image bg ext_aidpost2_sunset = get_image_7dl("bg/ext_med2_sunset.jpg")
    image bg ext_beach2_day = get_image_7dl("bg/ext_beach2_day.jpg")
    image bg ext_bus1 = get_image_7dl("bg/ext_bus1.jpg")
    image bg ext_bathhouse_day = get_image_7dl("bg/ext_bathhouse_day.jpg")
    image bg ext_winter_night = get_image_7dl("bg/outro/ext_winter_night.jpg")
    image bg ext_city_night = get_image_7dl("bg/outro/ext_city_night.jpg")
    image bg ext_dining_hall_near_snowy_day = get_image_7dl("bg/ext_dining_hall_near_snowy_day.jpg")
    image bg ext_clubs_sunset_rain = get_image_7dl("bg/ext_clubs_sunset_rain.jpg")
    image bg ext_earth = get_image_7dl("bg/outro/ext_earth.jpg")
    image bg ext_entrance_night_clear = get_image_7dl("bg/ext_entrance_night_clear.jpg")
    image bg ext_entrance_night_clear_closed = get_image_7dl("bg/ext_entrance_night_clear_closed.jpg")
    image bg ext_entrance_winter = get_image_7dl("bg/ext_entrance_winter.jpg")
    image bg ext_houses_night = get_image_7dl("bg/ext_houses_night.jpg")
    image bg ext_houses_rainy_day = get_image_7dl("bg/ext_houses_rainy_day.jpg")
    image bg ext_houses_snowy_day = get_image_7dl("bg/ext_houses_snowy_day.jpg")
    image bg ext_house_of_mt_snowy_day = get_image_7dl("bg/ext_house_of_mt_snowy_day.jpg")
    image bg ext_house_of_un_night = get_image_7dl("bg/ext_house_of_un_night.jpg")
    image bg ext_lib_sunset = get_image_7dl("bg/ext_lib_sunset.jpg")
    image bg ext_lake_day = get_image_7dl("bg/ext_lake_day.png")
    image bg ext_lake_night = get_image_7dl("bg/ext_lake_night.png")
    image bg ext_lake_sunset = get_image_7dl("bg/ext_lake_sunset.png")
    image bg ext_musclub_night = get_image_7dl("bg/ext_musclub_night.jpg")
    image bg ext_musclub_snowy_day = get_image_7dl("bg/ext_musclub_snowy_day.jpg")
    image bg ext_mv2 = get_image_7dl("bg/ext_mv2.jpg")
    image bg ext_night_sky = get_image_7dl("bg/ext_night_sky.jpg")
    image bg ext_old_building_day = get_image_7dl("bg/ext_old_building_day.jpg")
    image bg ext_playground2 = get_image_7dl("bg/ext_playground2.jpg")
    image bg ext_road_sunset = get_image_7dl("bg/ext_road_sunset.jpg")
    image bg ext_seashore = get_image_7dl("bg/ext_seashore.jpg")
    image bg ext_warehouse_day = get_image_7dl("bg/ext_warehouse_day.jpg")
    image bg ext_shower_day = get_image_7dl("bg/ext_shower_day.jpg")
    image bg ext_sky = get_image_7dl("bg/ext_sky.jpg")
    image bg ext_sky2 = get_image_7dl("bg/ext_sky2.jpg")
    image bg ext_stage_big_sunset = get_image_7dl("bg/ext_stage_big_sunset.jpg")
    image bg ext_stage_big_clear_day = get_image_7dl("bg/ext_stage_big_clear_day.jpg")
    image bg ext_stage_near_clear = get_image_7dl("bg/ext_stage_near_clear.jpg")
    image bg ext_stand3 = get_image_7dl("bg/ext_stand3.jpg")
    image bg ext_stand_pr = get_image_7dl("bg/ext_stand_pr.jpg")
    image bg ext_square_rain_day = get_image_7dl("bg/ext_square_rain_day.jpg")
    image bg ext_square_sunset2 = get_image_7dl("bg/ext_square_sunset2.jpg")
    image bg ext_square_sunset3 = get_image_7dl("bg/ext_square_sunset3.jpg")
    image bg ext_tennis_court = get_image_7dl("bg/ext_tennis_court.jpg")
    image bg ext_underwater = get_image_7dl("bg/ext_underwater.jpg")
    image bg ext_un_hideout_day = get_image_7dl("bg/ext_un_hideout_day.png")
    image bg ext_un_hideout_night = get_image_7dl("bg/ext_un_hideout_night.png")
    image bg ext_un_hideout_sunset = get_image_7dl("bg/ext_un_hideout_sunset.png")
    image bg ext_un_house_with_un = get_image_7dl("bg/ext_un_house_with_un.jpg")
    image bg ext_volley_court = get_image_7dl("bg/ext_volley_court.jpg")
    image bg ext_warehouse_blurry = get_image_7dl("bg/ext_warehouse_blurry.jpg")
    image bg ext_warehouse_night = get_image_7dl("bg/ext_warehouse_night.jpg")
    image bg ext_warehouse2_day = get_image_7dl("bg/ext_warehouse2_day.jpg")
    image bg ext_washstand_night = get_image_7dl("bg/ext_washstand_night.jpg")
    image bg ext_winterpark = get_image_7dl("bg/outro/ext_winterpark.jpg")
    image bg ext_winterpark_sunset = get_image_7dl("bg/outro/ext_winterpark_sunset.jpg")
    
    image bg int_cafee = get_image_7dl("bg/int_cafee.jpg")
    image bg int_attic = get_image_7dl("bg/int_attic.jpg")
    image bg int_attic_ladder = get_image_7dl("bg/int_attic_ladder.jpg")
    image bg int_admins_window = get_image_7dl("bg/int_admins_window.jpg")
    image bg int_catacomb_door_fullbright = get_image_7dl("bg/int_catacomb_door_fullbright.jpg")
    image bg int_chief_office = get_image_7dl("bg/int_chief_office.jpg")
    image bg int_clubs_dj = get_image_7dl("bg/int_clubs_dj.jpg")
    image bg int_clubs_dj_night = get_image_7dl("bg/int_clubs_dj_night.jpg")
    image bg int_clubs_dj_night_nolight = get_image_7dl("bg/int_clubs_dj_night_nolight.jpg")
    image bg int_concert_room = get_image_7dl("bg/int_concert_room.jpg")
    image bg int_d3_hideout = get_image_7dl("bg/int_d3_hideout.jpg")
    image bg int_dining_hall_people_rain = get_image_7dl("bg/int_dinning_hall_people_rain.jpg")
    image bg int_dining_hall_rain = get_image_7dl("bg/int_dinning_hall_rain.jpg")
    image bg int_epilogue_bg = get_image_7dl("bg/outro/int_epilogue_bg.jpg")
    image bg int_extra_house = get_image_7dl("bg/int_extra_house.jpg")
    image bg int_extra_house_day = get_image_7dl("bg/int_extra_house_day.jpg")
    image bg int_hence_day = get_image_7dl("bg/int_hence_day.jpg")
    image bg int_hence_night = get_image_7dl("bg/int_hence_night.jpg")
    image bg int_home_lift = get_image_7dl("bg/int_home_lift.jpg")
    image bg int_intro_liaz = get_image_7dl("bg/int_intro_liaz.jpg")
    image bg int_looney_bin = get_image_7dl("bg/int_looney_bin.jpg")
    image bg int_looney_bin_nightmare = get_image_7dl("bg/int_looney_bin_nightmare.jpg")
    image bg int_mine_heart = get_image_7dl("bg/int_mine_heart.jpg")
    image bg int_mine_halt_left = get_image_7dl("bg/int_mine_halt_left.jpg")
    image bg int_mine_room2 = get_image_7dl("bg/int_mine_room2.jpg")
    image bg int_mine_water = get_image_7dl("bg/int_mine_water.jpg")
    image bg int_plats = get_image_7dl("bg/outro/int_plats.jpg")
    image bg int_sam_house_clean = get_image_7dl("bg/int_sam_house_clean.jpg")
    image bg int_shed_day = get_image_7dl("bg/int_warehouse_day.jpg")
    image bg int_shed_night = get_image_7dl("bg/int_warehouse_night.jpg")
    image bg int_sport = get_image_7dl("bg/int_sport.jpg")
    image bg int_sporthall_day = get_image_7dl("bg/int_sporthall_day.jpg")
    image bg int_sporthall_night = get_image_7dl("bg/int_sporthall_day.jpg")
    image bg int_store = get_image_7dl("bg/int_store.jpg")
    image bg int_toilet = get_image_7dl("bg/outro/int_toilet.jpg")
    
#Сценки ака CG

    
    image cg d1_me_dahell = get_image_7dl("cg/d1_me_dahell.jpg")
    image cg d1_mi_bus = get_image_7dl("cg/d1_mi_bus.jpg")
    image cg d1_mi_dv_bus = get_image_7dl("cg/d1_mi_dv_bus.jpg")
    image cg d1_sl_dinner_0_day = get_image_7dl("cg/d1_sl_dinner_0_day.jpg")
    image cg d1_sl_dinner_day = get_image_7dl("cg/d1_sl_dinner_day.jpg")
    image cg d1_un_book = get_image_7dl("cg/d1_un_book.jpg")
    image cg d1_un_book0 = get_image_7dl("cg/d1_un_book0.jpg")
    image cg d1_uv_bus = get_image_7dl("cg/d1_uv_bus.jpg")
    
    image cg d2_dv_boat_day = get_image_7dl("cg/d2_dv_boat_day.jpg")
    image cg d2_mt_slack = get_image_7dl("cg/d2_mt_slack.jpg")
    image cg d2_un_kissing = get_image_7dl("cg/d2_un_kissing.jpg")
    image cg d2_un_knees = get_image_7dl("cg/d2_un_knees.jpg")
    image cg d2_us_boat_escape = get_image_7dl("cg/d2_us_boat_escape.jpg")
    image cg d2_un_owlet_pioneer = get_image_7dl("cg/d2_un_owlet_pioneer.jpg")
    image cg d2_us_soccer_sunset = get_image_7dl("cg/d2_us_soccer_sunset.jpg")
    
    image cg d3_dv_alice_dj80 = get_image_7dl("cg/d3_dv_alice_dj80.jpg")
    image cg d3_disco_no_un = get_image_7dl("cg/d3_disco_no_un.jpg")
    image cg d3_me_mirror_white = get_image_7dl("cg/d3_me_mirror_white.jpg")
    image cg d3_me_mirror_bordo = get_image_7dl("cg/d3_me_mirror_bordo.jpg")
    image cg d3_sl_bath_unplaited = get_image_7dl("cg/d3_sl_bath_unplaited.jpg")
    image cg d3_sl_tease = get_image_7dl("cg/d3_sl_tease.jpg")
    image cg d3_sl_tease2 = get_image_7dl("cg/d3_sl_tease2.jpg")
    image cg d3_sl_dance_bordo = get_image_7dl("cg/d3_sl_dance_bordo.jpg")
    image cg d3_un_dance_bordo = get_image_7dl("cg/d3_un_dance_bordo.jpg")
    
    image cg d4_fz_catac_un = get_image_7dl("cg/d4_fz_catac_un.jpg")
    image cg d4_fz_catac_sl = get_image_7dl("cg/d4_fz_catac_sl.jpg")
    image cg d4_fz_catac_dv = get_image_7dl("cg/d4_fz_catac_dv.jpg")
    image cg d4_fz_catac_el = get_image_7dl("cg/d4_fz_catac_el.jpg")
    image cg d4_fz_camping = get_image_7dl("cg/d4_fz_camping.jpg")
    image cg d4_fz_un_reject = get_image_7dl("cg/d4_fz_un_reject.jpg")
    image cg d4_fz_un_sleep = get_image_7dl("cg/d4_fz_un_sleep.jpg")
    image cg d4_hatch_night = get_image_7dl("cg/d4_hatch_night.jpg")
    image cg d4_hatch_night_open = get_image_7dl("cg/d4_hatch_night_open.jpg")
    image cg d4_lineup_no_un = get_image_7dl("cg/d4_lineup_no_un.jpg")
    image cg d4_mi_dissapear1 = get_image_7dl("cg/d4_mi_dissapear1.jpg")
    image cg d4_mi_dissapear2 = get_image_7dl("cg/d4_mi_dissapear2.jpg")
    image cg d4_mi_dj_dancing = get_image_7dl("cg/d4_mi_dj_dancing.jpg")
    image cg d4_mi_dj_lib0 = get_image_7dl("cg/d4_mi_dj_lib0.jpg")
    image cg d4_mi_dj_lib = get_image_7dl("cg/d4_mi_dj_lib.jpg")
    image cg d4_mi_guitar_club = get_image_7dl("cg/d4_mi_guitar_club.jpg")
    image cg d4_mi_guitar_moon = get_image_7dl("cg/d4_mi_guitar_moon.jpg")
    image cg d4_mi_lineup = get_image_7dl("cg/d4_mi_lineup.jpg")
    image cg d4_mi_sup = get_image_7dl("cg/d4_mi_sup.jpg")
    image cg d4_sh_met = get_image_7dl("cg/d4_sh_met.jpg")
    image cg d4_sl_sleeping = get_image_7dl("cg/d4_sl_sleeping.jpg")
    image cg d4_sl_sleep = get_image_7dl("cg/d4_sl_sleep.jpg")
    image cg d4_sl_dnd = get_image_7dl("cg/d4_sl_dnd.jpg")
    image cg d4_un_7dl = get_image_7dl("cg/d4_un_7dl.jpg")
    image cg d4_volley_rage = get_image_7dl("cg/d4_volley_rage.jpg")
    
    image cg d5_me_Alisa = get_image_7dl("cg/d5_me_Alisa.jpg")
    image cg d5_mi_conv = get_image_7dl("cg/d5_mi_conv.jpg")
    image cg d5_mt_redress = get_image_7dl("cg/d5_mt_redress.jpg")
    image cg d5_boat_night_solo = get_image_7dl("cg/d5_boat_night_solo.jpg")
    image cg d5_rainy_idle = get_image_7dl("cg/d5_rainy_idle.jpg")
    image cg d5_sl_bed = get_image_7dl("cg/d5_sl_bed.jpg")
    image cg d5_sl_bench = get_image_7dl("cg/d5_sl_bench.jpg")
    image cg d5_sl_kissing = get_image_7dl("cg/d5_sl_kissing.png")
    image cg d5_sl_moon = get_image_7dl("cg/d5_sl_moon.jpg")
    image cg d5_sl_swimming = get_image_7dl("cg/d5_sl_swimming.jpg")
    image cg d5_un_7dl_film = get_image_7dl("cg/d5_un_7dl_film.jpg")
    image cg d5_un_7dl_boat = get_image_7dl("cg/d5_un_7dl_boat.jpg")
    image cg d5_un_7dl_bed = get_image_7dl("cg/d5_un_bed.jpg")
    image cg d5_un_rape_scene = get_image_7dl("cg/d5_un_rape_scene.jpg")
    image cg d5_un_cunni = get_image_7dl("cg/d5_un_cunni.jpg")
    
    image cg d6_dance_alt = get_image_7dl("cg/d6_dance_alt.jpg")
    image cg d6_dv_dance = get_image_7dl("cg/d6_dv_dance.jpg")
    image cg d6_mi_boat = get_image_7dl("cg/d6_mi_boat.jpg")
    image cg d6_mi_morning = get_image_7dl("cg/d6_mi_morning.jpg")
    image cg d6_mi_farewell = get_image_7dl("cg/d6_mi_farewell.jpg")
    image cg d6_mi_swimming = get_image_7dl("cg/d6_mi_swimming.jpg")
    image cg d6_sl_clean = get_image_7dl("cg/d6_sl_clean.jpg")
    image cg d6_sl_zettai = get_image_7dl("cg/d6_sl_zettai.jpg")
    image cg d6_un_evening_0 = get_image_7dl("cg/d6_un_evening_0.jpg")
    image cg d6_un_evening_3 = get_image_7dl("cg/d6_un_evening_3.jpg")
    image cg d6_un_evening_0_1 = get_image_7dl("cg/d6_un_evening_0_1.jpg")
    image cg d6_un_evening_0_2 = get_image_7dl("cg/d6_un_evening_0_2.jpg")
    
    image cg d7_dv_alice_dj = get_image_7dl("cg/d7_dv_alice_dj.jpg")
    image cg d7_dv_bed = get_image_7dl("cg/d7_dv_bed.jpg")
    image cg d7_dv_ep_red = get_image_7dl("cg/d7_dv_ep_red.jpg")
    image cg d7_dv_noir = get_image_7dl("cg/d7_dv_noir.jpg")
    image cg d7_dv_epilogue_kissing = get_image_7dl("cg/d7_dv_epilogue_kissing.jpg")
    image cg d7_dv_epilogue_bus = get_image_7dl("cg/d7_dv_epilogue_bus.jpg")
    image cg d7_dv_rf_reject = get_image_7dl("cg/d7_dv_rf_reject.jpg")
    image cg d7_frozen = get_image_7dl("cg/d7_frozen.jpg")
    
    image cg d7_leaving_no_sl = get_image_7dl("cg/d7_leaving_no_sl.jpg")
    image cg d7_leaving_no_sl_sam = get_image_7dl("cg/d7_leaving_no_sl_sam.jpg")
    image cg d7_mi_farewell = get_image_7dl("cg/d7_mi_farewell.jpg")
    image cg d7_mi_hugs = get_image_7dl("cg/d7_mi_hugs.jpg")
    image cg d7_mi_neutral = get_image_7dl("cg/d7_mi_neutral.jpg")
    image cg d7_mi_letter = get_image_7dl("cg/d7_mi_letter.jpg")
    image cg d7_mi_letter_rain = get_image_7dl("cg/d7_mi_letter_rain.jpg")
    image cg d7_mi_letter_rain_tears = get_image_7dl("cg/d7_mi_letter_rain_tears.jpg")
    image cg d7_mi_ramen = get_image_7dl("cg/d7_mi_ramen.jpg")
    image cg d7_mi_reenter = get_image_7dl("cg/d7_mi_reenter.jpg")
    image cg d7_mi_epilogue = get_image_7dl("cg/d7_mi_epilogue.jpg")
    image cg d7_mt_n2gether = get_image_7dl("cg/d7_mt_n2gether.jpg")
    image cg d7_sl_gonna_be_ok = get_image_7dl("cg/d7_sl_gonna_be_ok.jpg")
    image cg d7_sh_ai_4eva = get_image_7dl("cg/d7_sh_ai_4eva.jpg")
    image cg d7_trio = get_image_7dl("cg/d7_trio.jpg")
    image cg d7_un_epilogue = get_image_7dl("cg/d7_un_epilogue.jpg")
    image cg d7_un_epilogue_bad = get_image_7dl("cg/d7_un_epilogue_bad.jpg")
    image cg d7_un_epilogue_bad2 = get_image_7dl("cg/d7_un_epilogue_bad2.jpg")
    image cg d7_un_met = get_image_7dl("cg/d7_un_met.jpg")
    image cg d7_un_epilogue_d1 = get_image_7dl("cg/d7_un_epilogue_d1.jpg")
    image cg d7_un_epilogue_d2 = get_image_7dl("cg/d7_un_epilogue_d2.jpg")
    image cg d7_un_reanimation = get_image_7dl("cg/d7_un_reanimation.jpg")
    image cg d7_walkman = get_image_7dl("cg/d7_walkman.jpg")
    
#Турнир
    image alt_tournament_bg = get_image_7dl("gui/tournament/alt_tournament_bg.png")
    
    image alt_KS_censor = get_image_7dl("alt_KS_censor.png")
    image alt_KS_censor2 = get_image_7dl("alt_KS_censor2.png")
    
    image sl_1_dv = get_image_7dl("sl_1_dv.png")
    image dv_playon = get_image_7dl("gui/tournament/dv_playon.png")
    image me_playon = get_image_7dl("gui/tournament/me_playon.png")
    image mi_playon = get_image_7dl("gui/tournament/mi_playon.png")
    image mz_playon = get_image_7dl("gui/tournament/mz_playon.png")
    image sh_playon = get_image_7dl("gui/tournament/sh_playon.png")
    image sl_playon = get_image_7dl("gui/tournament/sl_playon.png")
    image un_playon = get_image_7dl("gui/tournament/un_playon.png")
    image us_playon = get_image_7dl("gui/tournament/us_playon.png")
    
#Заглушки для wannabe-комикса
    image comix3 = get_image_7dl("gui/intro/comix3.png")
    image comix4 = get_image_7dl("gui/intro/comix4.png")
    image comix4 = get_image_7dl("gui/intro/comix4.png")

    image blind1_1 = get_image_7dl("gui/intro/blind1_1.png")
    image blind1_2 = get_image_7dl("gui/intro/blind1_2.png")
    image blind2_1 = get_image_7dl("gui/intro/blind2_1.png")
    image blind2_2 = get_image_7dl("gui/intro/blind2_2.png")
    image blind3_1 = get_image_7dl("gui/intro/blind3_1.png")
    image blind3_2 = get_image_7dl("gui/intro/blind3_2.png")
    image blind4_1 = get_image_7dl("gui/intro/blind4_1.png")
    image blind4_2 = get_image_7dl("gui/intro/blind4_2.png")
    image blind4_3 = get_image_7dl("gui/intro/blind4_3.png")
    image blind4_4 = get_image_7dl("gui/intro/blind4_4.png")
    image ldb_blind = get_image_7dl("gui/ldb_blind.png")
#Интро
    
    image believe_in_pain = get_image_7dl("gui/intro/believe_in_pain.jpg")
    image gameover = get_image_7dl("gui/intro/gameover.jpg")
    image intro_drisch = get_image_7dl("gui/intro/intro_dr.png")
    image intro_herc = get_image_7dl("gui/intro/intro_herc.png")
    image intro_loki = get_image_7dl("gui/intro/intro_loki.png")
    image spill_gray = get_image_7dl("gui/intro/spill_gray.png")
    image spill_red = get_image_7dl("gui/intro/spill_red.png")
    image acm_a = get_image_7dl("gui/intro/acm_a.png")
    image acm_l = get_image_7dl("gui/intro/acm_l.png")
    image acm_m = get_image_7dl("gui/intro/acm_m.png")
    image acm_o = get_image_7dl("gui/intro/acm_o.png")
    image acm_s = get_image_7dl("gui/intro/acm_s.png")
    image acm_u = get_image_7dl("gui/intro/acm_u.png")
    image name_dv = get_image_7dl("gui/intro/name_dv.png")
    image name_un = get_image_7dl("gui/intro/name_un.png")
    image name_mi = get_image_7dl("gui/intro/name_mi.png")
    image name_mt = get_image_7dl("gui/intro/name_mt.png")
    image name_sl = get_image_7dl("gui/intro/name_sl.png")
    image name_us = get_image_7dl("gui/intro/name_us.png")
    image rain_overlay = get_image_7dl("gui/rain_overlay.png")
    image intro_transparent = get_image_7dl("gui/intro/intro_transparent.png")
#Элементы интерфейса
    image blackout = get_image_7dl("gui/blackout.png")
    image blackout2 = get_image_7dl("gui/blackout2.png")
    image blackout_exh = get_image_7dl("gui/blackout_exh.png")
    image blackout_exh2 = get_image_7dl("gui/blackout_exh2.png")
    image blackout_exh3 = get_image_7dl("gui/blackout_exh3.png")
    image genda_portrait = get_image_7dl("gui/genda_portrait.png")
    
    image anim_exhausted: 
        get_image_7dl("gui/blackout_exh2.png")
        0.03 #Задержка
        get_image_7dl("gui/blackout_exh3.png")
        0.03 #Задержка
        get_image_7dl("gui/blackout_exh2.png")
        0.03 #Задержка
        get_image_7dl("gui/blackout_exh3.png")
        0.03 #Задержка
        get_image_7dl("gui/blackout_exh2.png")
        0.03 #Задержка
        repeat # Не убирать
    
#Карта
#    image bg map = store.map_pics["bg map"]
#    image widget map = store.map_pics["widget map"]

#Скачки по карте
    image dvsem_el = get_image_7dl("gui/maps/dvsem_el.png")
    image eye_s = get_image_7dl("gui/eye_s.png")
#Сотик
    image frame = get_image_7dl("gui/phone/frame.png")
    image cam_ui = get_image_7dl("gui/phone/cam_ui.png")
#Пека
    image laptop = get_image_7dl("gui/laptop/laptop.png")
    image laptop_dv_bad = get_image_7dl("gui/laptop/laptop_dv_bad.png")
    image laptop_dv_bad_dr = get_image_7dl("gui/laptop/laptop_dv_bad_dr.png")
    image laptop_dv_bad_loki = get_image_7dl("gui/laptop/laptop_dv_bad_loki.png")
    image laptop_dv_bad_herc = get_image_7dl("gui/laptop/laptop_dv_bad_herc.png")
    image laptop_mi_anyone = get_image_7dl("gui/laptop/laptop_mi_anyone.png")
    image laptop_prologue = get_image_7dl("gui/laptop/laptop_prologue.png")
    image laptop_un_epilogue = get_image_7dl("gui/laptop/laptop_un_epilogue.png")
    image laptop_un_epilogue_dr = get_image_7dl("gui/laptop/laptop_un_epilogue_dr.png")
    image laptop_un_epilogue_loki = get_image_7dl("gui/laptop/laptop_un_epilogue_loki.png")
    image laptop_un_epilogue_herc = get_image_7dl("gui/laptop/laptop_un_epilogue_herc.png")
    
    image anim_laptop: 
        get_image_7dl("gui/laptop/laptop_prologue.png") 
        0.3 #Задержка
        get_image_7dl("gui/laptop/laptop_prologue_weak.png")
        0.25
        get_image_7dl("gui/laptop/laptop_prologue_mid.png")
        0.25
        get_image_7dl("gui/laptop/laptop_prologue_strong.png")
        0.2
        get_image_7dl("gui/laptop/laptop_prologue_mid.png")
        0.25
        get_image_7dl("gui/laptop/laptop_prologue_weak.png")
        0.3
        repeat # Не убирать
    
#Камень-ножницы-бумага    
    image miku_paper = get_image_7dl("gui/rps/miku_paper.png")
    image miku_rock = get_image_7dl("gui/rps/miku_rock.png")
    image miku_scissor = get_image_7dl("gui/rps/miku_scissor.png")
    image sam_paper = get_image_7dl("gui/rps/sam_paper.png")
    image sam_rock = get_image_7dl("gui/rps/sam_rock.png")
    image sam_scissor = get_image_7dl("gui/rps/sam_scissor.png")
#Ачивы+лого 
    image achieve1 = get_image_7dl("gui/achieve1.png")
    image achieve_worth = get_image_7dl("gui/achieve_worth.png")
    image achieve_beagod = get_image_7dl("gui/achieve_beagod.png")
    image acm_logo = get_image_7dl("gui/acm_logo1.png")
    image acm_logo_bad = get_image_7dl("gui/acm_logo_bad.png")
    image acm_logo_drunk = get_image_7dl("gui/acm_logo_drunk.png")
    image acm_logo_dv_morethanlife = get_image_7dl("gui/acm_logo_dv_morethanlife.png")
    image acm_logo_meetmethere = get_image_7dl("gui/acm_logo_meetmethere.png")
    image acm_logo_dv_ussr_good = get_image_7dl("gui/acm_logo_dv_ussr_good.png")
    image acm_logo_green_fairy = get_image_7dl("gui/acm_logo_green_fairy.png")
    image acm_logo_gohome = get_image_7dl("gui/acm_logo_gohome.png")
    image acm_logo_mi_allyours = get_image_7dl("gui/acm_logo_mi_allyours.png")
    image acm_logo_namiki = get_image_7dl("gui/acm_logo_namiki.png")
    image acm_logo_lamp = get_image_7dl("gui/acm_logo_lamp.png")
    image acm_logo_ok = get_image_7dl("gui/acm_logo_ok.png")
    image acm_logo_ricochet = get_image_7dl("gui/acm_logo_ricochet.png")
    image acm_logo_sui = get_image_7dl("gui/acm_logo_sui.png")
    image acm_logo_sl_fantazm = get_image_7dl("gui/acm_logo_sl_fantazm.png")
    image acm_logo_theresnoway = get_image_7dl("gui/acm_logo_theresnoway.png")
    image acm_logo_tillend = get_image_7dl("gui/acm_logo_tillend.png")
    image acm_logo_tulpa = get_image_7dl("gui/acm_logo_tulpa.png")
    image acm_logo_sl_lone = get_image_7dl("gui/acm_logo_sl_lone.png")
    image acm_logo_sl_good = get_image_7dl("gui/acm_logo_sl_good.png")
    image acm_logo_sl_bad = get_image_7dl("gui/acm_logo_sl_bad.png")
    image acm_logo_sl_ok = get_image_7dl("gui/acm_logo_sl_ok.png")
    image acm_logo_sl_same_place = get_image_7dl("gui/acm_logo_sl_same_place.png")
    image acm_logo_sl_too_late = get_image_7dl("gui/acm_logo_sl_too_late.png")
    image acm_logo_un_good = get_image_7dl("gui/acm_logo_un_good.png")
    image acm_qte = get_image_7dl("gui/acm_qte.png")
    image myst_mh = get_image_7dl("gui/myst_mh.png")
    image dreamgirl_overlay = get_image_7dl("gui/dreamgirl_overlay.png")
    image wet1 = get_image_7dl("gui/wet1.png")
    image volley_fight = get_image_7dl("gui/volley_fight.png")
    
#Картинки с использованием прозрачности и прочая спрайтовость
    image dv normal flipped = Transform("dv normal pioneer", xzoom=-1.0)
    image dv normal flipped far = Transform("dv normal pioneer far", xzoom=-1.0)
    image sl normal flipped = Transform("sl normal pioneer", xzoom=-1.0)
    image sl serious flipped = Transform("sl serious pioneer", xzoom=-1.0)
    #image d3_miku_dance_blush flipped = Transform("d3_miku_dance_blush", xzoom=-1.0)
    image uv shade3 sized = Transform("uv shade3", zoom=.4)
    image uv shade4 sized = Transform("uv shade4", zoom=.4)
    image sl_trench = get_sprite_7dl("misc/sl_trench.png")
    image sl_trench2 = get_sprite_7dl("misc/sl_trench2.png")
    image cotocomb_lighter = get_sprite_7dl("misc/cotocomb_lighter.png")
    image d4_cat_door_frame = get_sprite_7dl("misc/d4_cat_door_frame.png")
    image d6_miku_cries = get_sprite_7dl("misc/d6_miku_cries.png")
    #image d3_miku_dance = get_sprite_7dl("misc/d3_miku_dance.png") - TODO: под заказ
    #image d3_miku_dance_blush = get_sprite_7dl("misc/d3_miku_dance_blush.png")
    #image d3_miku_dance_bordo = get_sprite_7dl("misc/d3_miku_dance_bordo.png")
    image mi_ru = get_sprite_7dl("misc/mi_ru.png")
    image mt_bus = get_sprite_7dl("misc/mt_bus.png")
    image uvao_d1 = get_sprite_7dl("misc/uvao_d1.png")
    image dv_mt = get_sprite_7dl("misc/dv_mt.png")
    image dv_us_volley = get_sprite_7dl("misc/dv_us_volley.png")
    
#Dnd
    image alt_cat_map_wireframe = get_image_7dl("gui/dnd/alt_cat_map_wireframe.png")
    image alt_cat_map = get_image_7dl("gui/dnd/alt_cat_map.png")
    image alt_cat_map_pathfinding = get_image_7dl("gui/dnd/alt_cat_map_pathfinding.png")
    
    
    
#Звучок
#ambience
    $ ambience_elevator = get_ambience_7dl("ambience_elevator.ogg")
    $ ambience_explosive_post = get_ambience_7dl("ambience_explosive_post.ogg")
    $ ambience_safe = get_ambience_7dl("ambience_safe.ogg")
    $ ambience_volley = get_ambience_7dl("ambience_volley.ogg")
    $ ambience_railroad = get_ambience_7dl("railroad_ambience.ogg")
    $ ambience_rain = get_ambience_7dl("ambience_rain_loop.ogg")
    $ ambience_concert = get_ambience_7dl("tellyourworld_concert.ogg")
#music
    $ old_kiss = get_music_7dl("25_years_old_kiss.ogg")
    $ alice_theme = get_music_7dl("alice_theme.ogg")
    $ anglegrinder = get_music_7dl("anglegrinder.ogg")
    $ areyouabully = get_music_7dl("areyouabully.ogg")
    $ are_you_there = get_music_7dl("are_you_there.ogg")
    $ ask_you_out = get_music_7dl("ask_you_out.ogg")
    $ closetoyou = get_music_7dl("closetoyou.ogg")
    $ clueless_hope = get_music_7dl("clueless_hope.ogg")
    $ beth = get_music_7dl("beth.ogg")
    $ betray_vol1 = get_music_7dl("betray_vol1.ogg")
    $ but_why = get_music_7dl("but_why.ogg")
    $ breath_me = get_music_7dl("breath_me.ogg")
    $ beasteye = get_music_7dl("beasteye.ogg")
    $ danceagain = get_music_7dl("danceagain.ogg")
    $ deadman = get_music_7dl("deadman.ogg")
    $ dead_silence = get_music_7dl("dead_silence.ogg")
    $ wonderful_faraway = get_music_7dl("wonderful_faraway.ogg")
    $ dropit = get_music_7dl("dropit.ogg")
    $ dv_guitar = get_music_7dl("dv_guitar.ogg")
    $ es_downmix = get_music_7dl("es_downmix.ogg")
    $ Everlasting_Summer_Alice = get_music_7dl("Everlasting_Summer_Alice.ogg")
    $ frostwithoutyou = get_music_7dl("frostwithoutyou.ogg")
    $ genki = get_music_7dl("genki.ogg")
    $ gonna_be_ok = get_music_7dl("gonna_be_ok.ogg")
    $ happy_ending = get_music_7dl("happy_ending.ogg")
    $ herc_death = get_music_7dl("herc_death.ogg")
    $ iwantmagic = get_music_7dl("iwantmagic.ogg") #Бенефис Шурика
    $ iamsadiamsorry3 = get_music_7dl("iamsadiamsorry3.ogg")#Сценка с ОД
    $ iamsadiamsorry2 = get_music_7dl("iamsadiamsorry2.ogg")
    $ iamsadiamsorry = get_music_7dl("iamsadiamsorry.ogg")
    $ knock = get_music_7dl("knock.ogg")
    $ kiss_you = get_music_7dl("kiss_you.ogg")
    $ lastlight_guitar = get_music_7dl("lastlight_guitar.ogg")
    $ lastlight_piano = get_music_7dl("lastlight_piano.ogg")
    $ lazy_olga = get_music_7dl("lazy_olga.ogg")
    $ lonesome_shepherd = get_music_7dl("lonesome_shepherd.ogg")
    $ liliac_ball = get_music_7dl("liliac_ball.ogg")
    $ loki_on_3 = get_music_7dl("loki_on_3.ogg")
    $ there_you_are = get_music_7dl("loop_there_you_are!.ogg")
    $ lost_without_you = get_music_7dl("lost_without_you.ogg")
    $ ltyh = get_music_7dl("ltyh.ogg")
    $ lunar_anguish = get_music_7dl("lunar_anguish.ogg")
    $ lyrica_sg = get_music_7dl("lyrica_sg.ogg")
    $ lynn = get_music_7dl("lynn.ogg")
    $ mammal = get_music_7dl("mammal.ogg")
    $ me2ost = get_music_7dl("me2ost.ogg")
    $ melancholy_sun = get_music_7dl("melancholy_sun.ogg")
    $ misery = get_music_7dl("misery.ogg")
    $ nookie = get_music_7dl("nookie.ogg")
    $ no_hope_left = get_music_7dl("no_hope_left.ogg")
    $ nowyouseeme = get_music_7dl("nowyouseeme.ogg")
    $ ourfirstmet = get_music_7dl("ourfirstmet.ogg") 
    $ outer = get_music_7dl("Outer_Science.ogg") 
    $ out_of_your_tier = get_music_7dl("out_of_your_tier.ogg") 
    $ pathways = get_music_7dl("pathways.ogg")
    $ personal_jesus = get_music_7dl("personal_jesus.ogg")
    $ Please_Reprise = get_music_7dl("Please_Reprise.ogg")
    $ polyhymnia_intro = get_music_7dl("polyhymnia_intro.ogg")
    $ polyhymnia_main = get_music_7dl("polyhymnia_main.ogg")
    $ ppk = get_music_7dl("ppk.ogg")
    $ prologue_1 = get_music_7dl("prologue_1.ogg")
    $ promise_to_meet_you = get_music_7dl("promise_to_meet_you.ogg")
    $ rewind = get_music_7dl("rewind.ogg")
    $ rightroad = get_music_7dl("rightroad.ogg")
    $ ritual2 = get_music_7dl("ritual2.ogg")
    $ scorpions = get_music_7dl("scorpions.ogg")
    $ silent_angel = get_music_7dl("silent_angel.ogg")
    $ sh_ai_rejuv = get_music_7dl("sh_ai_rejuv.ogg")
    $ shape_of_my_heart = get_music_7dl("shape_of_my_heart.ogg")
    $ sheiscool = get_music_7dl("sheiscool.ogg")
    $ shehasgone = get_music_7dl("shehasgone.ogg")
    $ slavyas_fantazm = get_music_7dl("slavyas_fantazm.ogg")
    $ sky_feather = get_music_7dl("sky_feather.ogg")
    $ sneakupon = get_music_7dl("sneakupon.ogg")
    $ so_cold = get_music_7dl("so_cold.ogg")#Мику и антагонизм
    $ splin = get_music_7dl("splin1.ogg")
    $ stilllovingyou = get_music_7dl("stilllovingyou.ogg")
    $ take_my_hand = get_music_7dl("take_my_hand.ogg")
    $ tears_of = get_music_7dl("tears_of.ogg")
    $ tellyourworld = get_music_7dl("tellyourworld.ogg")
    $ tender_song = get_music_7dl("tender_song.ogg")
    $ the_way = get_music_7dl("the_way.ogg")
    $ tilltheend = get_music_7dl("tilltheend.ogg")
    $ too_quiet = get_music_7dl("too_quiet.ogg")
    $ uncertainity = get_music_7dl("uncertainity.ogg")
    $ unfinished_life = get_music_7dl("unfinished_life.ogg")
    $ unforgotten = get_music_7dl("unforgotten.ogg")
    $ vale = get_music_7dl("vale.ogg")
    $ vampire = get_music_7dl("vampire.ogg")
    $ walkingaway = get_music_7dl("walkingaway.ogg")
    $ wheres_wonderland = get_music_7dl("wheres_wonderland.ogg")
    $ will_you = get_music_7dl("will_you.ogg")
    $ you_were_late = get_music_7dl("you_were_late.ogg")
#sfx
    $ apple_bite = get_sfx_7dl("apple_bite.ogg")
    $ aunl = get_sfx_7dl("aunl.ogg")
    $ bed_squeak = get_sfx_7dl("bed_squeak.ogg")
    $ breath = get_sfx_7dl("breath.ogg")
    $ blanket = get_sfx_7dl("blanket.ogg")
    $ deagle_shot = get_sfx_7dl("deagle_shot.ogg")
    $ eat_horn = get_sfx_7dl("eat_horn.ogg")
    $ ghmm = get_sfx_7dl("ghm.ogg")
    $ hedgehog = get_sfx_7dl("hedgehog.ogg")
    $ highfive = get_sfx_7dl("highfive.ogg")
    $ KBtyping = get_sfx_7dl("KBtyping.ogg")
    $ kissing_sound = get_sfx_7dl("kissing_sound.ogg")
    $ makarych = get_sfx_7dl("makarych.ogg")
    $ miku_stomping = get_sfx_7dl("miku_stomping.ogg")
    $ metal_hit_on_metal = get_sfx_7dl("metal_hit_on_metal.ogg")
    $ mpbt = get_sfx_7dl("mpbt.ogg")
    $ phone_feedback = get_sfx_7dl("phone_feedback.ogg")
    $ pouring = get_sfx_7dl("pouring.ogg")
    $ pup_bark = get_sfx_7dl("pup_bark.ogg")
    $ raindrops_radio = get_sfx_7dl("raindrops_radio.ogg")
    $ ringtone = get_sfx_7dl("ringtone.ogg")
    #Озвучка заголовка роли by Noir Сычёв
    $ role_drisch = get_sfx_7dl("role_drisch.ogg")
    $ role_herc = get_sfx_7dl("role_herc.ogg")
    $ role_loki = get_sfx_7dl("role_loki.ogg")
    $ sigh_out = get_sfx_7dl("sigh_out.ogg")
    $ snap = get_sfx_7dl("snap.ogg")
    $ stahp = get_sfx_7dl("stahp.ogg")
    $ tousche = get_sfx_7dl("tousche.ogg")
    $ train_depart = get_sfx_7dl("train_depart.ogg")
    $ train_income = get_sfx_7dl("train_income.ogg")
    $ nesmogla = get_sfx_7dl("nesmogla.ogg")
    $ volley_hit = get_sfx_7dl("volley_hit.ogg")
    $ wakeup_horn = get_sfx_7dl("wakeup_horn.ogg")
    $ wakeup = get_sfx_7dl("wakeup.ogg")
    $ white_noise = get_sfx_7dl("white_noise.ogg")
    $ window_glass_break = get_sfx_7dl("window_glass_break.ogg")


#########################################################    
#Ресы для кошочки
#bg:
    #ext's
    image bg ext_dining_hall_away_night_uvao1 = get_image_7dl("bg/ext_dining_hall_away_night_uvao1.jpg")
    image bg ext_dining_hall_away_night_uvao2 = get_image_7dl("bg/ext_dining_hall_away_night_uvao2.jpg")
    image bg ext_old_building_day = get_image_7dl("bg/ext_old_building_day.jpg")
    image bg ext_washstand_sunset = get_image_7dl("bg/ext_washstand_sunset.jpg")
    
    #int's
    image bg int_old_building_day_uvao = get_image_7dl("bg/int_old_building_day_uvao.jpg")
    image bg int_mines_halt_spotlight = get_image_7dl("bg/int_mines_halt_spotlight.jpg")
    image bg int_catacombs_entrance_light = get_image_7dl("bg/int_catacombs_entrance_light.jpg")
    image bg int_mine_exit_day = get_image_7dl("bg/int_mine_exit_day.jpg")
    image bg int_catacombs_door_light2 = get_image_7dl("bg/int_catacombs_door_light2.jpg")
    image bg int_sleep_hentai_office = get_image_7dl("bg/int_sleep_hentai_office.jpg")
    image bg int_sleep_hentai_office2 = get_image_7dl("bg/int_sleep_hentai_office2.jpg")
    #Анимка спичек
    image match_lights: 
        contains: 
            get_image_7dl('matches_tone.png') # оранжевенькая тонировка путь 1
            additive 1.0
        contains: 
            get_image_7dl('matches_lightmask.png') # путь2
            xalign 0.5 yalign 1.0 
            function random_zoom # дрожание огонька
            repeat
    # Анимка бесконечных спичек
    image bg int_mine_crossroad_matches: 
        "bg black" with fade2
        0.5
        block:
            block:
                contains: 
                    'bg int_mine_crossroad' with fade2 
                contains: 
                    get_image_7dl('matches_tone.png') #путь3
                    additive 1.0
                contains: 
                    get_image_7dl('matches_lightmask.png') #путь4
                    xalign 0.5 yalign 1.0 
                    function random_zoom
                    repeat            
            4.0
            "bg black" with fade2
            4.0
            repeat
                   
#cg
    image cg d4_uv_pioner_lib_hiding = get_image_7dl("cg/d4_uv_pioner_lib_hiding.jpg")
    image cg d5_uv_photo_city = get_image_7dl("cg/d5_uv_photo_city.jpg")
    image cg d5_uv_photo_galaxy = get_image_7dl("cg/d5_uv_photo_galaxy.jpg")
    image cg d4_uv_bunker_hentai = get_image_7dl("cg/d4_uv_bunker_hentai.jpg")
    # Кошочка авторства Орики
    image uv_new_hentai1 = get_image_extra7dl("cg/epilogue_uv_hentai_1.jpg")
    image uv_new_hentai2 = get_image_extra7dl("cg/epilogue_uv_hentai_2.jpg")

    
#sounds
    $ phone_vibro = get_sfx_7dl("vibration-smartphone.ogg")
    $ match_lights =get_sfx_7dl("lighting-a-match.ogg")
    $ silence = get_sfx_7dl("silence_4sec.ogg")
    $ my_chrysalis_highwayman = get_music_7dl("my_chrysalis_highwayman.ogg")

