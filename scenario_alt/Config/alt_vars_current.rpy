label alt_day4_neutral_init:
    # начало для Сыче-хаба д.4:
    $ alt_day4_neu_us_snake = False
    $ alt_day4_neu_back_pack = False
    $ alt_day4_neu_date = 0
    $ alt_day4_neu_mt_diary = False
    $ alt_day4_neu_mt_fire = False
    $ alt_day4_neu_mt_volley = False
    $ alt_day4_neu_mt_songs = False
    $ alt_day4_neu_sl_pup = False
    $ alt_day4_neu_mi_event = False
    return
    
label alt_day5_neutral_init:
    $ alt_day5_neu_candle = 0
    $ alt_day5_neu_mt_diary = False
    $ alt_day5_neu_potato = False
    $ alt_day5_neu_mt_voyeur = 0
    $ alt_day5_neu_sl_voyeur = False
    $ alt_day4_fz_sh = locals().get('alt_day4_fz_sh', False)
    python:
        try:
            alt_day4_neu_us_snake
        except NameError:
            renpy.call('alt_day4_neutral_init')
    return

label alt_day4_sl_init:
    # переменные для Славки
    $ alt_day4_sl_un_rej = False
    $ alt_day4_sl_tut_iz = False
    $ alt_day4_sl_tut = False
    $ alt_day4_sl_tut_lf = False
    $ alt_day4_sl_lf_solo = 0 #Сольные поиски
    return

label alt_day5_sl_wh_init:
    return

label alt_day5_sl_init:
    $ alt_day4_fz_sh = locals().get('alt_day4_fz_sh', 0)
    $ alt_day5_sl_extra_house = False
    $ alt_day5_sl_tan = False
    return

label alt_day6_sl_init:
    $ alt_day6_sl_arc = 0
    $ alt_day6_sl_repel = False
    $ alt_day6_sl_shirt = False
    return

label alt_day7_sl_init:
    $ alt_day7_sl_map_progress = 0
    $ alt_day7_sl_code = False
    return

label alt_sl_cl_init:
    call alt_day4_sl_init
    call alt_day5_sl_init
    call alt_day6_sl_init
    call alt_day7_sl_init
    return

label alt_day4_mi_dj_init:
    # начало для Мику-DJ д.4 - рут готовый
    $ alt_day4_mi_dj_hedg = False
    $ alt_day4_mi_dj_blackmailed = False
    $ alt_day4_mi_dj_sl_repet = False
    $ alt_day4_mi_dj_reasons = False
    return

label alt_day5_mi_dj_init:
    # начало для Мику-DJ д.5
    $ alt_day5_mi_dj_musclub_visited = False
    $ alt_day5_mi_dj_entrance_visited = False
    $ alt_day4_mi_dj_sl_repet = False
    $ alt_day5_mi_dj_estrade_visited = False
    $ alt_day5_mi_dj_home_visited = False
    $ alt_day5_mi_dj_necessary_done = 0
    $ alt_day5_mi_dj_dv_blade = False
    $ alt_day5_mi_dj_voyeur = 0
    $ alt_day5_mi_dj_favorite_song = False
    $ alt_day5_mi_dj_hentai = False
    $ alt_day5_mi_dj_dv_kissing = False
    $ alt_day5_mi_dj_forgive = False
    $ alt_day5_mi_dj_cut = False
    # Транзит с Мику-7ДЛ
    $ alt_day4_mi_7dl_sh_key = locals().get('alt_day4_mi_7dl_sh_key', 0)
    call alt_day4_mi_dj_init
    return

label alt_day6_mi_dj_init:
    # начало для Мику-DJ д.6
    $ alt_day6_mi_dj_sl_evil = False
    $ alt_day6_mi_dj_dv_evil = False
    $ alt_day6_mi_dj_sl_alt_day1_dv_feed = False
    $ alt_day6_mi_dj_un_evil = False
    $ alt_day6_mi_dj_key = False
    $ alt_day6_mi_dj_walkman = False
    $ alt_day6_mi_dj_hentai1 = False
    $ alt_day6_mi_dj_hentai2 = False
    $ alt_day6_mi_dj_hentai_catch = False
    $ alt_day6_mi_dj_letmeout = False
    $ alt_day6_mi_dj_letmestay = False
    $ alt_day6_mi_dj_no_hentai = False
    $ alt_day6_mi_dj_rejected = False
    return

label alt_day4_un_7dl_init:
    # начало для Леночки-7дл, д.4 - рут готовый.
    $ alt_un_old_hentai = False #Включить старую версию хентая?
    $ alt_day4_un_7dl_morning_searching = False
    $ alt_day4_un_7dl_ducks = False
    $ alt_day4_un_7dl_ba_alerted = False
    $ alt_day4_un_7dl_dv_calm = False
    $ alt_day4_un_7dl_un_calm = False
    $ alt_day4_un_7dl_dv_us_explosives = False
    $ alt_day4_un_7dl_hen1 = False
    return

label alt_day5_un_7dl_init:
    # начало для Леночки-7дл, д.5
    $ alt_day5_un_7dl_square_duty = False
    $ alt_day5_un_7dl_club_day = False
    $ alt_day5_un_7dl_cube = False
    $ alt_day5_un_7dl_sl_un_washing = False
    $ alt_day5_un_7dl_solo_washing = False
    $ alt_day5_un_7dl_sl_fight = False
    return

label alt_day6_un_7dl_init:
    $ alt_day6_un_7dl_agreed = False
    return

label alt_un_7dl_init:
    call alt_day4_un_7dl_init
    call alt_day5_un_7dl_init
    call alt_day6_un_7dl_init
    return

label alt_day5_dv_init:
    $ alt_day4_fz_sh = locals().get('alt_day4_fz_sh', 0)

label alt_day4_un_fz_init:
    # начало для Леночки-7дл, д.6
    $ alt_day6_un_7dl_agreed = False
    
    # начало для Леночки-ФЗ, д.4
    $ alt_day4_fz_play = 0
    $ alt_day4_fz_cards = False
    $ alt_day4_fz_sl_walk = False
    $ alt_day4_fz_sl_wish = False
    $ alt_day4_fz_girl_off_1 = False
    $ alt_day4_fz_girl_off_2 = False
    $ alt_day4_fz_girl_off_3 = False
    $ alt_day4_fz_girl_off_5 = False
    $ alt_day4_fz_sh = 0
    $ alt_day4_fz_un_letter = False
    $ alt_day4_fz_girl_off = 0
    return

label alt_day4_mi_7dl_init:
    # начало для Мику-7дл
    $ alt_day4_mi_7dl_sh_key_stolen = False
    $ alt_day4_mi_7dl_sh_key_quest = False
    $ alt_day4_mi_7dl_sh_key = False
    $ alt_day4_mi_7dl_harmed = False
    $ alt_day4_mi_7dl_sl_estrada_help = False
    $ alt_day4_mi_7dl_radiocheck = False
    $ alt_day4_mi_7dl_song = False
    $ alt_day4_mi_7dl_locked = False
    $ alt_day4_mi_7dl_un_bf = False
    $ alt_day4_mi_7dl_voyeur = False
    $ alt_day4_mi_7dl_mi2sl_transit = 0
    return

label alt_day4_mi_init:
    # начало для Мику-классик д.4
    $ alt_day4_mi_mi2midj_transit = False
    $ alt_day4_mi_vodka = False
    $ alt_day4_mi_clothes = False
    $ alt_day4_mi_bake = False
    $ alt_day4_mi_coal = False
    $ alt_day4_mi_senio = 0
    $ alt_day4_mi_photo_slavya = False
    $ alt_day4_mi_photo = False
    $ alt_day4_mi_rival_won = False
    return

label alt_day4_dv_7dl_init:
    # начало для Алисы-7дл д.4
    $ alt_day4_dv_7dl_extra_key = False #Слямзил ключик или нет?
    $ alt_day4_dv_7dl_walkman_presented = False
    $ alt_day4_dv_7dl_portwine = False
    $ alt_day4_dv_7dl_vodka = False
    $ alt_day4_dv_7dl_ba_conv = False
    $ alt_day4_dv_7dl_mt_drugs = False
    $ alt_day4_dv_7dl_drank_vodka = False
    $ alt_day4_dv_7dl_hentai = False
    return

label alt_day6_dv_7dl_init:
    # начало для Алисы-7дл, д.6. Пятого дня нет, ибо нефиг.
    $ alt_day6_dv_7dl_mi_route = False
    $ alt_day6_dv_7dl_sl_route = False
    $ alt_day6_dv_7dl_transit = False
    $ alt_day6_dv_7dl_key = False
    $ alt_day6_dv_7dl_key_hentai = 0
    $ alt_day6_dv_7dl_alco_hentai = 0
    $ alt_day6_dv_7dl_ba_hentai = 0
    $ alt_day6_dv_7dl_hentai = 0
    $ alt_day6_dv_7dl_dance = 0
    return

label alt_day7_dv_7dl_init:
    # начало для Алисы-7дл, д.7. Пятого дня нет, ибо нефиг.
    $ alt_day7_dv_7dl_check = locals().get('alt_day7_dv_7dl_check', 0)
    return

label alt_dv_7dl_init:
    call alt_day4_dv_7dl_init
    call alt_day6_dv_7dl_init
    call alt_day7_dv_7dl_init
    return

label alt_day4_uvao_init:
    #Все иниты кошкорута находятся здесь
    $ alt_uvao_true = False
    $ alt_uvao_D4_viola_morning = False
    $ alt_uvao_D4_concert = False
    $ alt_uvao_D4_lunch_un = False
    $ alt_uvao_D4_lunch_sl = False
    $ alt_uvao_D4_supper_cs = False
    $ alt_uvao_D5_hentai = False
    $ alt_uvao_D5_sh_mines = False
    $ alt_uvao_D5_evening_sl = False
    $ alt_uvao_D5_evening_dv_un = False
    return

##########
#С переменными наигрались, теперь посмотрим, куда нашего Семёна занесло
label alt_day3_slots:
    stop music
    stop ambience
    stop sound_loop
    window hide
    scene black
    with dissolve
    
label alt_day3_router_dv:
    if lp_dv >= 11:
        "Мне снилась Алиса…"
        window hide
        if alt_day3_dv_reunion:
            $ routetag = "dv7dl"
            jump alt_day4_dv_7dl_start
#        elif alt_day3_dancing == 3 and alt_day3_technoquest_st3 == 2:
#            $ routetag = "dv"
#            jump alt_day4_dv_start
#        elif alt_day3_dv_dj:
#            $ routetag = "dv"
#            jump alt_day4_dv_dj_start
        else:
            jump alt_day3_router_un
            
label alt_day3_router_un:
    if (lp_un >= 12) or (alt_day2_date == 132):
        "Мне снилась Лена…"
        window hide
        if (alt_day3_un_med_help == 1) and (lp_un >= 12):
            $ routetag = "un7dl"
            jump alt_day4_un_7dl_start
        elif (alt_day2_date == 132) and (alt_day3_dancing == 132):
            $ routetag = "un7dl"
            jump alt_day4_un_fz_start
#        else:
#            $ routetag = "un"
#            jump alt_day4_un_start
    else:
        jump alt_day3_router_mi

label alt_day3_router_mi:
    if lp_mi >= 13:
        if (alt_day3_dancing == 41 or alt_day3_dancing == 40) and not alt_day3_mi_dj:
            "Мне снилась Мику…"
            window hide
            $ routetag = "mi7dl"
            jump alt_day4_mi_start_7dl
#        elif alt_day3_technoquest_st3 == 2:
#            "Мне снилась Мику…"
#            window hide
#            $ routetag = "mi7dl"
#            jump alt_day4_mi_start
        elif alt_day3_mi_dj:
            "Мне снилась Мику…"
            "Правда, не очень долго."
            window hide
            $ routetag = "mi7dl"
            jump alt_day4_mi_dj_start
    else:
        jump alt_day3_router_sl

label alt_day3_router_sl:
    if alt_day3_sl_day_event2 and lp_sl >= 13:
        "Мне снилась Славя…"
        window hide
        if alt_day3_technoquest_st3 == 2:
            $ routetag = "sl"
            jump alt_day4_sl_start
        else:
            $ routetag = "sl7dl"
            jump alt_day4_sl_7dl_start
    else:
        jump alt_day3_router_neutral
    
label alt_day3_router_neutral:
    if alt_day3_uvao_spotted:
        $ routetag = "uv"
        "Сон был тревожным…"
        window hide
        jump alt_day4_start_uvao #Начало ЮВАО-рута
    else:
        if alt_day3_technoquest_st3 == 2:
            pass
        else:
            "И спалось мне не очень."
        window hide
        jump alt_day4_neu_start

